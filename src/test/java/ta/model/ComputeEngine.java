package ta.model;

import java.util.Objects;

public class ComputeEngine {
    private String numberOfInstances;
    private String software;
    private String machineClass;
    private String series;
    private String machineType;
    private String localSSD;
    private String location;
    private String numberOfGPUs;
    private String GPUtype;
    private String usage;

    public ComputeEngine(String numberOfInstances, String software, String machineClass, String series, String machineType, String localSSD, String location, String numberOfGPUs, String GPUtype, String usage) {
        this.numberOfInstances = numberOfInstances;
        this.software = software;
        this.machineClass = machineClass;
        this.series = series;
        this.machineType = machineType;
        this.localSSD = localSSD;
        this.location = location;
        this.numberOfGPUs = numberOfGPUs;
        this.GPUtype = GPUtype;
        this.usage = usage;
    }

    public String getNumberOfInstances() {
        return numberOfInstances;
    }

    public void setNumberOfInstances(String numberOfInstances) {
        this.numberOfInstances = numberOfInstances;
    }

    public String getSoftware() {
        return software;
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public String getMachineClass() {
        return machineClass;
    }

    public void setMachineClass(String machineClass) {
        this.machineClass = machineClass;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getMachineType() {
        return machineType;
    }

    public void setMachineType(String machineType) {
        this.machineType = machineType;
    }

    public String getLocalSSD() {
        return localSSD;
    }

    public void setLocalSSD(String localSSD) {
        this.localSSD = localSSD;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getNumberOfGPUs() {
        return numberOfGPUs;
    }

    public void setNumberOfGPUs(String numberOfGPUs) {
        this.numberOfGPUs = numberOfGPUs;
    }

    public String getGPUtype() { return GPUtype;}

    public void setGPUtype(String GPUtype) {
        this.GPUtype = GPUtype;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    @Override
    public String toString() {
        return "ComputeEngine{" +
                "numberOfInstances='" + numberOfInstances + '\'' +
                ", software='" + software + '\'' +
                ", machineClass='" + machineClass + '\'' +
                ", series='" + series + '\'' +
                ", machineType='" + machineType + '\'' +
                ", localSSD='" + localSSD + '\'' +
                ", location='" + location + '\'' +
                ", numberOfGPUs='" + numberOfGPUs + '\'' +
                ", GPUtype='" + GPUtype + '\'' +
                ", usage='" + usage + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComputeEngine that = (ComputeEngine) o;
        return Objects.equals(
                getNumberOfInstances(), that.getNumberOfInstances())
                && Objects.equals(getSoftware(), that.getSoftware())
                && Objects.equals(getMachineClass(), that.getMachineClass())
                && Objects.equals(getSeries(), that.getSeries())
                && Objects.equals(getMachineType(), that.getMachineType())
                && Objects.equals(getLocalSSD(), that.getLocalSSD())
                && Objects.equals(getLocation(), that.getLocation())
                && Objects.equals(getNumberOfGPUs(), that.getNumberOfGPUs())
                && Objects.equals(getGPUtype(), that.getGPUtype())
                && Objects.equals(getUsage(), that.getUsage());
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                getNumberOfInstances(),
                getSoftware(),
                getMachineClass(),
                getSeries(),
                getMachineType(),
                getLocalSSD(),
                getLocation(),
                getNumberOfGPUs(),
                getGPUtype(),
                getUsage());
    }
}
