package ta.page;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.checkerframework.checker.units.qual.C;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.security.Key;
import java.util.ArrayList;

public class MailPage extends BasePage{

    private final Logger logger = LogManager.getRootLogger();
    private final String PAGE_URL = "https://yopmail.com/uk/";

    public MailPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(xpath = "//input[@name = 'login']")
    private WebElement loginInputField;

    @FindBy(xpath = "//h2[contains(text(), 'Estimated Monthly Cost:')]")
    private WebElement costNumberData;

    @FindBy(xpath = "//*[@id='ifmail']")
    private WebElement firstIframe;
    public MailPage openPage(){
        driver.navigate().to(PAGE_URL);
        logger.info("Mail page opened");
        return this;
    }

    public MailPage inputEmailInLoginInputField(String emailAddress){
        loginInputField.sendKeys(emailAddress, Keys.ENTER);
        return this;
    }
    public String getMassageData(){
        return costNumberData.getText();
    }
    public MailPage switchToTheEmailFrame(){
        driver.switchTo().frame(firstIframe);
        logger.info("Switched to the first frame");
        return this;
    }
}
