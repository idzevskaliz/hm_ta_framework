package ta.page;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchResultPage extends BasePage{

    private final Logger logger = LogManager.getRootLogger();

    @FindBy(xpath = "//a[@class = 'gs-title']")
    private WebElement firstSearchResultLink;
    public SearchResultPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    public GCPCalculatorPage goToResultPage(){
        implicitWait(WAIT_TIMEOUT_SECONDS);
        firstSearchResultLink.click();
        return new GCPCalculatorPage(driver);
    }
}
