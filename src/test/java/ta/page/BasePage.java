package ta.page;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public abstract class BasePage {
    protected WebDriver driver;

    protected final int WAIT_TIMEOUT_SECONDS = 30;

    protected BasePage(WebDriver driver){this.driver = driver;}

    public void waitForElementToBeClickable(long timeOut, WebElement element) {
        new WebDriverWait(driver, timeOut).until(
                webDriver -> ExpectedConditions.elementToBeClickable(element));
    }
    public void waitForPageLoadComplete(long timeOut) {
        new WebDriverWait(driver, timeOut).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
    }
    public void waitVisibilityOfElement(long timeToWait, WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, timeToWait);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void waitForAttributeToBe(long timeToWait, WebElement element, String attribute, String state) {
        WebDriverWait wait = new WebDriverWait(driver, timeToWait);
        wait.until(ExpectedConditions.attributeToBe(element,attribute, state));
    }

    public void implicitWait(long timeToWait) {
        driver.manage().timeouts().implicitlyWait(timeToWait, TimeUnit.SECONDS);
    }

    public void actionMoveToElement(WebElement locator) {
        Actions actions = new Actions(driver);
        actions.moveToElement(locator);
        actions.perform();
    }
    public void scrollToWebElement(WebElement webElement) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", webElement);
    }
}
