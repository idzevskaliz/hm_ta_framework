package ta.page;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.reporters.jq.Main;

public class MainPage extends BasePage{

    private final Logger logger = LogManager.getRootLogger();
    private final String PAGE_URL = "https://cloud.google.com/";

    public MainPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(xpath = "//div[@class = 'devsite-searchbox']")
    private WebElement searchButton;

    @FindBy(xpath = "//input[contains(@class, 'search-field')]")
    private WebElement inputField;

    public MainPage openPage(){
        driver.navigate().to(PAGE_URL);
        logger.info("Main page opened");
        return this;
    }
    public SearchResultPage searchInField(String searchWord){
        searchButton.click();
        waitVisibilityOfElement(WAIT_TIMEOUT_SECONDS,inputField);
        inputField.sendKeys(searchWord);
        inputField.sendKeys(Keys.ENTER);
        logger.info("Search word entered");
        return new SearchResultPage(driver);
    }
}
