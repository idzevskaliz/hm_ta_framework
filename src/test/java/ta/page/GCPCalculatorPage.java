package ta.page;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ta.model.ComputeEngine;

import java.util.ArrayList;
import java.util.List;

public class GCPCalculatorPage extends BasePage{

    private final Logger logger = LogManager.getRootLogger();
    private final String PAGE_URL = "https://cloud.google.com/products/calculator";

    public GCPCalculatorPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    @FindBy(xpath = "//form[@name = 'ComputeEngineForm']//input[@name = 'quantity']")
    private WebElement numberOfInstancesField;

    @FindBy(xpath = "//*[@id='select_container_97']//md-option/div[contains(@class, 'md-text')]")
    private List<WebElement> locationList;

    @FindBy(xpath = "//md-select[@placeholder ='Datacenter location']//span/div")
    private WebElement locationInput;

    @FindBy(xpath = "//md-select[@placeholder ='Series']//span/div")
    private WebElement seriesInput;

    @FindBy(xpath = "//*[@id='select_container_93']//md-option/div[contains(@class, 'md-text')]")
    private List<WebElement> seriesList;

    @FindBy(xpath = "//md-select[@placeholder = 'Instance type']//span//div")
    private WebElement machineTypeInput;

    @FindBy(xpath = "//*[@id='select_container_95']//md-option/div[contains(@class, 'md-text')]")
    private List<WebElement> machineTypeList;

    @FindBy(xpath = "//md-select[@placeholder = 'Local SSD']//span/div")
    public WebElement ssdInput;

    @FindBy(xpath = "//*[@id='select_container_393']//md-option/div[contains(@class, 'md-text')]")
    public List<WebElement> ssdList;

    @FindBy(xpath = "//md-select[@placeholder = 'Committed usage']//span/div[@class = 'md-text']")
    private WebElement usageInput;

    @FindBy(xpath = "//*[@id='select_container_104']//md-option/div[@class = 'md-text']")
    private List<WebElement> usageList;

    @FindBy(xpath = "//md-card-content//div[9]//md-input-container/md-checkbox//div[@class = 'md-label']")
    private WebElement gpuCheckbox;

    @FindBy(xpath = "//md-select[@placeholder = 'Number of GPUs']//span/div")
    private WebElement numberOfGPUsInput;

    @FindBy(xpath = "//*[@id='select_container_432']//md-content//div[contains(@class,'md-text')]")
    private List<WebElement> numberOfGPUsList;

    @FindBy(xpath = "//md-select[@placeholder = 'GPU type']//span/div")
    private WebElement gpuTypeInput;

    @FindBy(xpath = "//*[@id='select_container_434']//md-content//div[contains(@class,'md-text')]")
    private List<WebElement> gpuTypeList;

    @FindBy(xpath = "//button[@aria-label = 'Add to Estimate']")
    private List<WebElement> addButton;

    @FindBy(xpath = "//*[@id='resultBlock']/md-card/md-card-content//h2/b")
    private WebElement totalEstimateField;

    @FindBy(xpath = "//*[@id='cloud-site']/devsite-iframe/iframe")
    private WebElement firstIframe;

    @FindBy(xpath = "//*[@id='select_94']")
    private WebElement machineTypeAria;

    @FindBy(xpath = "//*[@id='select_container_432']")
    private WebElement numberOfGPUsArea;

    @FindBy(xpath = "//*[@id='select_container_97']")
    private WebElement locationArea;

    @FindBy(xpath = "//*[@id='select_container_393']")
    private WebElement ssdArea;

    @FindBy(xpath = "//*[@id='select_container_434']")
    private WebElement gpuTypeArea;

    @FindBy(xpath = "//*[@id='select_container_104']")
    private WebElement usageArea;

    @FindBy(xpath = "//button[@aria-label = 'Email Estimate']")
    private WebElement emailEstimateButton;

    @FindBy(xpath = "//input[@type = 'email']")
    private WebElement emailInputField;

    @FindBy(xpath = "//button[@aria-label = 'Send Email']")
    private WebElement sendEmailButton;

    public GCPCalculatorPage openPage(){
        driver.navigate().to(PAGE_URL);
        logger.info("GCP Calculator page opened");
        return this;
    }

    public String getCurrentUrl(){
        return driver.getCurrentUrl();
    }

    public GCPCalculatorPage switchToTheFormFrame(){
        driver.switchTo().frame(firstIframe);
        logger.info("Switched to the first frame");
        driver.switchTo().frame("myFrame");
        logger.info("Swirched to 'myframe'");
        return this;
    }
    public GCPCalculatorPage fillComputeEngineForm(ComputeEngine entity){
        numberOfInstancesField.sendKeys(entity.getNumberOfInstances());
        logger.info("Number of instances set");

        seriesInput.click();
        logger.info("Series list is opened");
        for (WebElement element: seriesList) {
            if(element.getAttribute("innerText").equals(entity.getSeries())){
                element.click();
            }
        }
        logger.info("Series set");

        machineTypeInput.click();
        waitForAttributeToBe(WAIT_TIMEOUT_SECONDS, machineTypeAria, "aria-expanded", "true");
        logger.info("Machine type list is opened");
        implicitWait(WAIT_TIMEOUT_SECONDS);
        for (WebElement element: machineTypeList) {
            if(element.getText().equals(entity.getMachineType())){
                element.click();
            }
        }
        logger.info("Machine type set");
        waitForAttributeToBe(WAIT_TIMEOUT_SECONDS, machineTypeAria, "aria-expanded", "false");

        gpuCheckbox.click();
        implicitWait(WAIT_TIMEOUT_SECONDS);
        logger.info("gpu button is clicked");

        numberOfGPUsInput.click();
        waitForAttributeToBe(WAIT_TIMEOUT_SECONDS, numberOfGPUsArea, "aria-hidden", "false");
        logger.info("Number of GPUs list is opened");
        for (WebElement element: numberOfGPUsList) {
            if(element.getAttribute("innerText").equals(entity.getNumberOfGPUs())){
                element.click();
            }
        }
        logger.info("Number of GPUs set");
        waitForAttributeToBe(WAIT_TIMEOUT_SECONDS, numberOfGPUsArea, "aria-hidden", "true");

        gpuTypeInput.click();
        waitForAttributeToBe(WAIT_TIMEOUT_SECONDS, gpuTypeArea, "aria-hidden", "false");
        logger.info("GPUs type list is opened");
        for (WebElement element: gpuTypeList) {
            if(element.getAttribute("innerText").equals(entity.getGPUtype())){
                element.click();
            }
        }
        waitForAttributeToBe(WAIT_TIMEOUT_SECONDS, gpuTypeArea, "aria-hidden", "true");
        logger.info("Type of GPUs set");

        usageInput.click();
        waitForAttributeToBe(WAIT_TIMEOUT_SECONDS, usageArea, "aria-hidden", "false");
        for (WebElement element: usageList) {
            if(element.getAttribute("innerText").equals(entity.getUsage())){
                element.click();
            }
        }
        waitForAttributeToBe(WAIT_TIMEOUT_SECONDS, usageArea, "aria-hidden", "true");

        ssdInput.click();
        waitForAttributeToBe(WAIT_TIMEOUT_SECONDS, ssdArea, "aria-hidden", "false");
        logger.info("SSD list is opened");
        for (WebElement element: ssdList) {
            if(element.getAttribute("innerText").equals(entity.getLocalSSD())){
                element.click();
            }
        }
        waitForAttributeToBe(WAIT_TIMEOUT_SECONDS, ssdArea, "aria-hidden", "true");
        logger.info("SSD set");

        locationInput.click();
        logger.info("Location is opened");
        waitForAttributeToBe(WAIT_TIMEOUT_SECONDS, locationArea, "aria-hidden", "false");
        for(WebElement element: locationList) {
            if (element.getAttribute("innerText").equals(entity.getLocation())){
                element.click();
            }
        }
        waitForAttributeToBe(WAIT_TIMEOUT_SECONDS, locationArea, "aria-hidden", "true");
        logger.info("Location set");

        implicitWait(WAIT_TIMEOUT_SECONDS);
        addButton.get(0).click();
        return this;
    }
    public String getTotalEstimate(){
        implicitWait(WAIT_TIMEOUT_SECONDS);
        return totalEstimateField.getText();
    }
    public GCPCalculatorPage fillEmailEstimateForm(String emailAddress){
        implicitWait(WAIT_TIMEOUT_SECONDS);
        emailEstimateButton.click();
        implicitWait(WAIT_TIMEOUT_SECONDS);
        emailInputField.sendKeys(emailAddress);
        waitForElementToBeClickable(WAIT_TIMEOUT_SECONDS,sendEmailButton);
        sendEmailButton.click();
        return this;
    }
    public MailPage openYopMailPage(){
        driver.switchTo().newWindow(WindowType.TAB);
        return new MailPage(driver);
    }

}
