package ta.test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ta.model.ComputeEngine;
import ta.page.GCPCalculatorPage;
import ta.page.MainPage;
import ta.service.TestDataReader;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class RegressionTests extends BaseTest{

    @DataProvider(name = "create")
    public static Object[] withCredentialsFromProperty(){
        return new Object[]{new ComputeEngine(
                "4", "", "", "N1",
                "n1-standard-8 (vCPUs: 8, RAM: 30GB)", "2x375 GB",
                "Frankfurt (europe-west3)", "1",
                "NVIDIA Tesla P4", "1 Year")
        };
    }

    @Test(dataProvider = "create")
    public void googleCloudPlatformPricingCalculatorEstimate(ComputeEngine engine){
        String email = TestDataReader.getTestData("testdata.computeEngine.emailAddress");
        String estimatedSum = new MainPage(driver)
                .openPage()
                .searchInField("Google Cloud Platform Pricing Calculator")
                .goToResultPage()
                .switchToTheFormFrame()
                .fillComputeEngineForm(engine)
                .fillEmailEstimateForm(email)
                .openYopMailPage()
                .openPage()
                .inputEmailInLoginInputField(email)
                .switchToTheEmailFrame()
                .getMassageData();
        assertThat(estimatedSum, is(containsString("USD 1,083.33")));
    }
}
