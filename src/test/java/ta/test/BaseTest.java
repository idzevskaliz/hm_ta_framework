package ta.test;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import ta.driver.Driver;
import ta.utils.TestListener;

@Listeners({TestListener.class})
public class BaseTest {
    protected WebDriver driver;

    @BeforeMethod()
    public void SetUp(){driver = Driver.getDriver();}

    @AfterMethod(alwaysRun = true)
    public void stopBrowser(){Driver.closeDriver();}
}
